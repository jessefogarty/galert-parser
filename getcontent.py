#!/usr/bin/python3
import sqlite3
import requests
import re
from bs4 import BeautifulSoup
#from tqdm import tqdm

conn = sqlite3.connect('gfeed.db')
cur = conn.cursor()

articles = cur.execute('''SELECT id, link FROM articles''').fetchall()

for links in articles:
    id = links[0]
    link = links[1]
    # if CBC link
    if re.search('cbc.ca', link):
        html = requests.get(link)
        soup = BeautifulSoup(html.content, 'lxml')
        # bodytext = soup.find('div', {'class':'story'}).get_text()
        print('CBC article')
    elif re.search('thestar.com', link):
        html = requests.get(link)
        soup = BeautifulSoup(html.content, 'lxml')
        # TODO: Parse thestar soup
        print('Toronto Star Article')
    elif re.search('globeandmail.com', link):
        html = requests.get(link)
        soup = BeautifulSoup(html.content, 'lxml')
        # TODO: parse g&m soup
        print('globe and mail article')
    elif re.search('ctvnews.ca', link):
        html = requests.get(link)
        soup = BeautifulSoup(html.content, 'lxml')
        # TODO: parse ctv soup
        print('ctvnews article')
    elif re.search('nationalpost.com', link):
        html = requests.get(link)
        soup = BeautifulSoup(html.content, 'lxml')
        # TODO: parse nationalpost soup
        print('nationalpost article')
    elif re.search('torontosun.com')
